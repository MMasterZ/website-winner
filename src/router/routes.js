const routes = [{
    path: "/loading",
    component: () => import("pages/loading.vue"),
    name: "loading"
  },
  {
    path: "/admin",
    component: () => import("pages/admin.vue"),
    name: "admin"
  },
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [{
        path: "/",
        component: () => import("pages/home.vue"),
        name: "home"
      },
      // NOTE  หน้าระบบออนไลน์
      {
        path: "/online",
        component: () => import("pages/online.vue"),
        name: "online"
      },
      // NOTE หน้าเรียนรู้ด้วยตัวเอง
      // แยกเข้าแบบออนไลน์
      {
        path: "/online/selflearning",
        component: () => import("pages/selflearning.vue"),
        name: "onlineselflearning"
      },
      // แยกเข้าแบบโรงเรียน
      {
        path: "/school/selflearning",
        component: () => import("pages/selflearning.vue"),
        name: "schoolselflearning"
      },

      // NOTE  หน้าระบบนักเรียน
      {
        path: "/school",
        component: () => import("pages/school.vue"),
        name: "school"
      },

      // NOTE  หน้าโครงสร้างบทเรียนของ
      // แยกเข้าแบบออนไลน์
      {
        path: "/online/structure",
        component: () => import("pages/structure.vue"),
        name: "onlinestructure"
      },

      // แยกเข้าแบบโรงเรียน
      {
        path: "/school/structure",
        component: () => import("pages/structure.vue"),
        name: "schoolstructure"
      },

      // NOTE หน้าทักษะ
      // แยกเข้าแบบออนไลน์
      {
        path: "/online/multiskill",
        component: () => import("pages/multiskill.vue"),
        name: "onlinemultiskill"
      },
      // แยกเข้าแบบออนไลน์
      {
        path: "/school/multiskill",
        component: () => import("pages/multiskill.vue"),
        name: "schoolmultiskill"
      },

      // NOTE  หน้าเข้าสู่ระบบ
      {
        path: "/login",
        component: () => import("pages/login.vue"),
        name: "login"
      },
      // NOTE หน้าคอร์สเรียน
      {
        path: "/course",
        component: () => import("pages/course.vue"),
        name: "course"
      },
      {
        path: "/register",
        component: () => import("pages/register.vue"),
        name: "register"
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
